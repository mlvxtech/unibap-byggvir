# Unibap - ByggVir (Kaust/Simera) Project #

This repo is used for collaboration between Metaspectral and Unibap for the ByggVir (Kaust/Simera) project.

### Installation ###
Run `pip install -r requirements.txt` to install dependencies.

### Running ###

The `test.py` script performs the following:

* Generates test image (all ones) with dimensions matching the FPGA compressor core
* Compresses test image using Java model
* Decompresses compressed image using Java model
* Compares original vs decompressed images

### Compressing with Java model ###

````
java -jar ccsds123.jar -i <input image file> --input_header <ENVI header for input file> -o <compressed file> -e 0 --bitdepth 16 -c
````

### Decompressing with Java model ###

````
java -jar ccsds123.jar -i <compressed file> --input_header <ENVI header for input file> -o <decompressed image file> --output_header <ENVI header for decompressed file> -e 0 --bitdepth 16 -d
````
