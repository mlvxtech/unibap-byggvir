#!/usr/bin/python3
import subprocess
import time

import numpy as np
import spectral.io.envi as envi

# Create subproccess to execute call string
def runCallstring(callstring):
    startTime = time.time()
    subprocess.call(callstring, shell=True)
    timeElapsed = time.time()-startTime
    print('Total time (s): ' + str(timeElapsed))

# Image dimensions (NX and NZ are fixed, NY can vary)
NX = 1024
NY = 100
NZ = 32

# Generate test data
img = np.ones((NY, NX, NZ), dtype=np.uint16)
print(img.shape)
print(img.dtype)

# Save test data and ENVI header file, must be BIP
envi.save_image('img.hdr', img, force=True, interleave='bip', ext=".dat")

# Compress data with Java model
# -i <input image file>
# --input_header <ENVI header for input file>
# -o <compressed file>
# -e <error> (must be 0 for lossless)
# --bitdepth <sample bit depth> (must be 16)
# -c (compress)
runCallstring("java -jar ccsds123.jar -i img.dat --input_header img.hdr -o img.ccsds -e 0 --bitdepth 16 -c")

# Decompress data with Java model
# -i <compressed file>
# --input_header <ENVI header for input file>
# -o <decompressed image file>
# --output_header <ENVI header for decompressed file>
# -e <error> (must be 0 for lossless)
# --bitdepth <sample bit depth> (must be 16)
# -d (decompress)
runCallstring("java -jar ccsds123.jar -i img.ccsds --input_header img.hdr -o img-decomp.dat --output_header img-decomp.hdr -e 0 --bitdepth 16 -d")

# Compare decompressed vs original test data
img_decomp = envi.open('img-decomp.hdr', 'img-decomp.dat').asarray()
print ("Image shape: ", img_decomp.shape)
print ("Image sample type: ", img_decomp.dtype)
print ("Match =", (img == img_decomp).all())
